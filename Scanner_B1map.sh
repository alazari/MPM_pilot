#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/premotor/Alberto_Project/AlbertoProject_test9
mkdir -p $workDir/DAM

scanList="
20@40
"


for i in $scanList; do

  # separate the two scans
  B1map=/vols/Scratch/myelin/Mohamed/67/pdata/1/nifti/AlbertoProject_Test9_67_1_4.nii

  echo "converting scanner B1 units"

  fslmaths $B1map -mul 180 -div 3.141592 -div 90 -mul 100 $workDir/DAM/scanner_B1map

done

echo "done"

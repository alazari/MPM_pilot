#!/bin/bash

# courtesy of Nicole Eichert (code and documentation, 2018). Big thanks to Nicole! :D

# Before use:
# - copy this script to a location you prefer
# - edit the path and loop below
#   the looping variable here is subject ID, but it can also be scan name (e.g. W3T_2018_106_004).
# - edit the paths in the script my_hMRI_wrapper.sh

# log in to jalapeno as normal (using ssh -Y yourusername@jalapeno.fmrib.ox.ac.uk).
# call this function from the jalapeno terminal by typing:
# bash /vols/Scratch/neichert/myCode/LarynxCode/my_hMRI_wrapper_submit.sh

scanList="20210402_231711_AL8_AL8_MPM_1_3"


module add MATLAB
module add spm/12_R2017b

# cd ../../Data/MRdata/alazari

for scan in $scanList; do
  echo "$scan"
  # cd $scan
  # rm -r fieldmap_gre_2mm_208mm_6_1/
  # rm -r fieldmap_gre_2mm_208mm_7_1/
  # rm -r hMRI_NIFTI/
  # rm -r RFCorr_results/
  # cd ..
  #
  # cd ../../../Scratch/alazari/
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_MT.sh $scan
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_T1.sh $scan
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_PD.sh $scan
  # cd ../../Data/MRdata/alazari
done



Step 1. Identify and check acquisition of whole scan (scan_identify_v3.sh)

Step 2. Convert DICOMs to NIFTI using the hMRI toolbox (my_hMRI_DICOM_wrapper_submit.sh)

Step 3. Register repetitions to each other (register_repetitions.sh)

Step 4. Swap NIFTI header so that it is oriented correctly.

Step 5. Calculate and register DAM B1 map (Double_Angle_Mapping.sh and B1_register.sh)

Step 6. Run hMRI wrapper to calculate quantitative maps.

function hMRI_DICOM_wrapper(directory,outfolder)
%% point it at the highest level directory for the subject. Eg, if your structure is this:
% Subject
% -->Dir1 (dicoms of scan 1)
% -->Dir2 (dicoms of scan 2)
% --> etc
% then point it at "Subject"
List_of_all=dir(strcat(directory,filesep,'**/*.*'));
DICOMFILES={};
for i=1:size(List_of_all,1)
 DICOMFILES{i}=strcat(List_of_all(i).folder,filesep,List_of_all(i).name);
end
DICOMFILES=DICOMFILES';
% DICOM conversion using hMRI toolbox, so that JSON metadata is supplied
% too
spm('defaults','fmri');
spm_jobman('initcfg');
matlabbatch{1}.spm.tools.hmri.dicom.data = (DICOMFILES);
matlabbatch{1}.spm.tools.hmri.dicom.root= 'series';
matlabbatch{1}.spm.tools.hmri.dicom.outdir = {outfolder};
matlabbatch{1}.spm.tools.hmri.dicom.protfilter = '.*';
matlabbatch{1}.spm.tools.hmri.dicom.convopts.format = 'nii';
matlabbatch{1}.spm.tools.hmri.dicom.convopts.icedims = 0;
matlabbatch{1}.spm.tools.hmri.dicom.convopts.metaopts.mformat = 'sep';
spm_jobman('run',matlabbatch);
clear matlabbatch
% cleanup
disp ('------------------------DICOM Conversion Complete------------------------------------------');
end
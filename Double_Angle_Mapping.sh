#!/usr/bin/env bash

# definitions
subj="20210526_201302_AL11_MPM_1_2"
workDir=/vols/Scratch/premotor/MPM_rodent_data/"$subj"_NIFTI
mkdir -p $workDir/DAM

scanList="
63@68
"
# 54@18

for i in $scanList; do

  # separate the two scans
  Angle1=${i%@*} #with flip angle alpha
  Angle2=${i#*@} #with flip angle 2*alpha

  # separate the two scans
  S1=$workDir/MGE2D_DAM_025iso_TR75s_FA40_"$Angle1"0001/*.nii #with flip angle alpha
  S2=$workDir/MGE2D_DAM_025iso_TR75s_FA80_"$Angle2"0001/*.nii #with flip angle 2*alpha

  echo $S1
  echo $S2
  echo "performing DAM"

  fslmaths $S1 -mul 2 $workDir/DAM/doubleS1
  fslmaths $S2 -div $workDir/DAM/doubleS1 $workDir/DAM/S2_divided_by_doubleS1
  fslmaths $workDir/DAM/S2_divided_by_doubleS1 -acos $workDir/DAM/Alpha_from_arccosine_S2_S1
  fslmaths $workDir/DAM/S2_divided_by_doubleS1 -mul 180 -div 3.141592 -div 40 -mul 100 $workDir/DAM/B1map_DAM
  cp $S1 $workDir/DAM/B1map_struct.nii
  cp $workDir/MGE2D_DAM_025iso_TR75s_FA40_"$Angle1"0001/*.json $workDir/DAM/B1map_struct.json

done

echo "done"

#!/usr/bin/env bash

# This script prepares MPM echoes from multiple repetitions for hMRI

# definitions
workDir=/vols/Scratch/myelin/Mohamed/20210402_231711_AL8_AL8_MPM_1_3_NIFTI
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts

subj="20210402_231711_AL8_AL8_MPM_1_3"
maptype="MT T1 PD"


# crete new folders for output
outDir=$workDir/SubjectDIR_RepetitionAverage_NoNavigator
mkdir -p $outDir



# to change
# - reference echo also gets registered (although it may be better to keep it this way, so that the ref echo does not have more signal?)
# - later echoes lose signal

# - make a separate script for the averaging part?




for map in $maptype; do

  echo "$map"

  # rm $scriptDir/"$map"_"$subj".txt
  # ls -d $workDir/mt_MGE_Nav_15mt_MGE15_"$map"_TR50_* >> $scriptDir/"$map"_"$subj".txt
  # referenceScan=$(cat "$scriptDir"/"$map"_"$subj".txt | head -n 1)
  #
  #     # for each repetition
  #     for repetition in $(cat "$scriptDir"/"$map"_"$subj".txt); do
  #
  #     echo "$repetition"
  #
  #       # apply registration transform to all echoes
  #       for a in {1..8}; do
  #       echo "echo number $a"
  #       flirt2=`fsl_sub -q veryshort.q -l $scriptDir/logs cp $repetition/s2021*-0000"$a"-*.nii $repetition/coregistered_echo_"$a"`
  #       done
  #
  #     echo "done with $repetition"
  #
  #     done
  #
  # # crete new folder for the average of repetitions
  # mkdir -p $outDir/"$map"wDIR

  # average the echoes across repetitions into the output folder
  for a in {1..8}; do
  echo "echo number $a"
  # fslmerge1=`fsl_sub -q veryshort.q -j ${flirt2} -l $scriptDir/logs fslmerge -t $outDir/"$map"wDIR/repetition_sum_"$a".nii $workDir/mt_MGE_Nav_15mt_MGE15_"$map"_TR50_*/coregistered_echo_"$a".nii.gz`
  # fslmaths1=`fsl_sub -q veryshort.q -j ${fslmerge1} -l $scriptDir/logs fslmaths $outDir/"$map"wDIR/repetition_sum_"$a".nii -Tmean $outDir/"$map"wDIR/"$map"W_echo_mean_"$a".nii`
  # rm1=`fsl_sub -q veryshort.q -j ${fslmaths1} -l $scriptDir/logs rm $outDir/"$map"wDIR/repetition_sum_"$a".nii.gz`
  #
  # # Copy json from raw echo to the average of repetitions for each echo
  # cp1=`fsl_sub -q veryshort.q -j ${fslmaths1} -l $scriptDir/logs cp $referenceScan/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/`
  # mv1=`fsl_sub -q veryshort.q -j ${cp1} -l $scriptDir/logs mv $outDir/"$map"wDIR/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/"$map"W_echo_mean_"$a".json`
  mv $outDir/"$map"wDIR/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/"$map"W_echo_mean_"$a".json

  done

echo "done"

done

gunzip $outDir/*/*.nii.gz

# B1 mapping
mkdir -p $outDir/B1DIR
# B1DIR
# 	B1_struct.nii (the DAM40 file)
# 	B1_map.nii (scaled to center around 100)

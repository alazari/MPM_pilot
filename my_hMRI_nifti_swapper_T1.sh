#!/bin/bash

# Before use:
# - edit your inputs to hMRI_wrapper below!
# - edit the path to the hMRI_wrapper script in the 'cd' command ad the end

# don't run this script directly, but call it using the following script:
# /vols/Scratch/neichert/myCode/LarynxCode/my_hMRI_wrapper_submit.sh
# the my_hMRI_wrapper_submit.sh script will submit the matlab function to the queue

# define function to pass the inputs to the hMRI_wapper
my_hMRI_wrapper(){
scan=$1

echo "do "$scan
matlab -nojvm -nodesktop -nosplash -r "nifti_swapper('/vols/Scratch/myelin/Mohamed/"$scan"_NIFTI/mt_MGE_Nav_15mt_MGE15_T1_TR42_ETL8_FA30_800001','/vols/Scratch/myelin/Mohamed/"$scan"_NIFTI/SubjectDIR_RepetitionAverage_NoNavigator/T1wDIR')"

}

# for map extraction
# hMRI_wrapper_Daniel('/vols/Data/MRdata/alazari/"$scan"','/home/fs0/alazari/spm12/', 1, 0, 0)
# for segmentation
# hMRI_segment('/vols/Scratch/alazari/longitudinal_paTMS/MPM/"$scan"')

# here the function is actually called
# $1 represents the input we are giving to my_hMRI_wrapper.sh (e.g. '01')
cd /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/
my_hMRI_wrapper $1

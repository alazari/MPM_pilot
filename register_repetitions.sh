#!/usr/bin/env bash

# This script prepares MPM echoes from multiple repetitions for hMRI

# definitions
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts

subjList="20210517_175914_AL2_MPM_1_1"
maptype="MT T1 PD"

# to change
# - reference echo also gets registered (although it may be better to keep it this way, so that the ref echo does not have more signal?)
# - later echoes lose signal

# - make a separate script for the averaging part?

## DONE

# Test
#20201210_185653_AlbertoProject_Test9_1_9_Dcm"
#20210402_231711_AL8_AL8_MPM_1_3

# Full Scans
#20210402_231711_AL8_AL8_MPM_1_3
#20210526_201302_AL11_MPM_1_2

for subj in $subjList; do

# crete new folders for output
workDir=/vols/Scratch/premotor/MPM_rodent_data/"$subj"_NIFTI
outDir=$workDir/SubjectDIR_RepetitionAverage
mkdir -p $outDir

# B1 mapping
mkdir -p $outDir/B1DIR
# B1DIR
# 	B1_struct.nii (the DAM40 file)
# 	B1_map.nii (scaled to center around 100)

rm $scriptDir/acquisition_order_files/MT/MT_"$subj".txt
rm $scriptDir/acquisition_order_files/PD/PD_"$subj".txt
rm $scriptDir/acquisition_order_files/T1/T1_"$subj".txt

ls -d $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA6_ETL6_MT_* >> $scriptDir/acquisition_order_files/MT/MT_"$subj".txt
ls -d $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA6_ETL8_Nav1_* >> $scriptDir/acquisition_order_files/PD/PD_"$subj".txt
ls -d $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA30_* >> $scriptDir/acquisition_order_files/T1/T1_"$subj".txt

for map in $maptype; do

  echo "$map"

  referenceScan=$(cat "$scriptDir"/acquisition_order_files/"$map"/"$map"_"$subj".txt | head -n 1)

      # for each repetition
      for repetition in $(cat "$scriptDir"/acquisition_order_files/"$map"/"$map"_"$subj".txt); do

      echo "$repetition"

      # take average of echoes within the same repetition
      fslmerge -t $repetition/repetition_sum.nii $repetition/*.nii
      fslmaths $repetition/repetition_sum.nii -Tmean $repetition/repetition_average.nii
      rm $repetition/repetition_sum.nii.gz

      # register to the first repetition's average of echoes with 6DOF, Search cost set to normmi
      flirt1=`fsl_sub -q veryshort.q -l $scriptDir/logs flirt -in $repetition/repetition_average.nii -ref $referenceScan/repetition_average.nii -out $repetition/repetition_average_registered.nii -omat $repetition/repetition_average_transform.mat -interp spline -dof 6 -searchcost normmi -cost normmi`

        # apply registration transform to all echoes
        for a in {1..8}; do
        echo "echo number $a"
        flirt2=`fsl_sub -q veryshort.q -j ${flirt1} -l $scriptDir/logs flirt -in $repetition/s2021*-0000"$a"-*.nii -ref $referenceScan/repetition_average.nii -applyxfm -init $repetition/repetition_average_transform.mat -out $repetition/coregistered_echo_"$a" -interp spline -dof 6`
        done

      echo "done with $repetition"

      done

  # crete new folder for the average of repetitions
  mkdir -p $outDir/"$map"wDIR

  # Copy json from raw echo to the average of repetitions for each echo
  for a in {1..8}; do
  cp1=`fsl_sub -q veryshort.q -j ${flirt2} -l $scriptDir/logs cp $referenceScan/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/`
  mv1=`fsl_sub -q veryshort.q -j ${cp1} -l $scriptDir/logs mv $outDir/"$map"wDIR/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/"$map"W_echo_mean_"$a".json`
  done

echo "done"

done


# average the echoes across repetitions into the output folder
for a in {1..8}; do
echo "echo number $a"

# MT
fslmerge1=`fsl_sub -q veryshort.q -j ${flirt2} -l $scriptDir/logs fslmerge -t $outDir/MTwDIR/repetition_sum_"$a".nii $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA6_ETL6_MT_*/coregistered_echo_"$a".nii.gz`
fslmaths1=`fsl_sub -q veryshort.q -j ${fslmerge1} -l $scriptDir/logs fslmaths $outDir/MTwDIR/repetition_sum_"$a".nii -Tmean $outDir/MTwDIR/MTW_echo_mean_"$a".nii`
rm1=`fsl_sub -q veryshort.q -j ${fslmaths1} -l $scriptDir/logs rm $outDir/MTwDIR/repetition_sum_"$a".nii.gz`

# T1
fslmerge1=`fsl_sub -q veryshort.q -j ${flirt2} -l $scriptDir/logs fslmerge -t $outDir/T1wDIR/repetition_sum_"$a".nii $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA30_*/coregistered_echo_"$a".nii.gz`
fslmaths1=`fsl_sub -q veryshort.q -j ${fslmerge1} -l $scriptDir/logs fslmaths $outDir/T1wDIR/repetition_sum_"$a".nii -Tmean $outDir/T1wDIR/T1W_echo_mean_"$a".nii`
rm1=`fsl_sub -q veryshort.q -j ${fslmaths1} -l $scriptDir/logs rm $outDir/T1wDIR/repetition_sum_"$a".nii.gz`

# PD
fslmerge1=`fsl_sub -q veryshort.q -j ${flirt2} -l $scriptDir/logs fslmerge -t $outDir/PDwDIR/repetition_sum_"$a".nii $workDir/mt_MGE_Nav_15mt_MGE15_TR50_FA6_ETL8_Nav1_*/coregistered_echo_"$a".nii.gz`
fslmaths1=`fsl_sub -q veryshort.q -j ${fslmerge1} -l $scriptDir/logs fslmaths $outDir/PDwDIR/repetition_sum_"$a".nii -Tmean $outDir/PDwDIR/PDW_echo_mean_"$a".nii`
rm1=`fsl_sub -q veryshort.q -j ${fslmaths1} -l $scriptDir/logs rm $outDir/PDwDIR/repetition_sum_"$a".nii.gz`

done


done

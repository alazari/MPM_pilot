#!/usr/bin/env bash

# This script prepares MPM echoes from multiple repetitions for hMRI

# definitions
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts

subjList="20210526_201302_AL11_MPM_1_2"
maptype="MT T1 PD"

# to change
# - reference echo also gets registered (although it may be better to keep it this way, so that the ref echo does not have more signal?)
# - later echoes lose signal

# - make a separate script for the averaging part?

## DONE

# Test
#20201210_185653_AlbertoProject_Test9_1_9_Dcm"
#20210402_231711_AL8_AL8_MPM_1_3

# Full Scans
#20210402_231711_AL8_AL8_MPM_1_3
#20210526_201302_AL11_MPM_1_2
#20210517_175914_AL2_MPM_1_1

for subj in $subjList; do

# crete new folders for output
workDir=/vols/Scratch/premotor/MPM_rodent_data/"$subj"_NIFTI
outDir=$workDir/SubjectDIR_RepetitionAverage

for map in $maptype; do

  echo "$map"
  # Copy json from raw echo to the average of repetitions for each echo
  for a in {1..8}; do
  mv $outDir/"$map"wDIR/s2021*-0000"$a"-*.json $outDir/"$map"wDIR/"$map"W_echo_mean_"$a".json
  done

echo "done"

done

done


gunzip $outDir/*/*.nii.gz

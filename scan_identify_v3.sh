#!/usr/bin/env bash

# This script bets the functional scan and estimates the registration from functional to bssfp structural

# definitions
workDir=/vols/Scratch/premotor/MPM_rodent_data
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts/

subjList="20210526_201302_AL11_MPM_1_2"

# 20210521_170621_AL6_AL6_MPM_3_1_4
# 20210519_162602_AL3_2_MPM_1_1
# 20210517_175914_AL2_MPM_1_1

## DONE

# Test
#20201210_185653_AlbertoProject_Test9_1_9"

# Full Scans
#20210402_231711_AL8_AL8_MPM_1_3

for subj in $subjList; do

  rm $scriptDir/acquisition_order_files/acquisition_order_"$subj".txt

  echo "identifying scans for $i"

  # for a in 2; do

  for a in {1..90}; do

  echo "$a"

  # $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$a"/acqp
  # grep -w "ACQ_protocol_name=( 64 )" $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$a"/acqp
  name=$(sed -n '13p' $workDir/"$subj"/"$a"/acqp)
  echo "$a" "$name" >> $scriptDir/acquisition_order_files/acquisition_order_"$subj".txt

  done

  # only pick the latest acquisition for each scan
  # number1=$(grep "T2w_anat_res0.06" $scriptDir/acquisition_order_"$subj".txt | awk '{print $1}' | tail -1)
  # echo $number1
  #
  # number2=$(grep "QSM_MGE_IES3ms_20TE" $scriptDir/acquisition_order_"$subj".txt | awk '{print $1}' | tail -1)
  # echo $number2
  #
  # number3=$(grep "win_DtiEpi_01_BlipDown" $scriptDir/acquisition_order_"$subj".txt | awk '{print $1}' | tail -1)
  # echo $number3
  #
  # number4=$(grep "win_DtiEpi_01_b2.5k" $scriptDir/acquisition_order_"$subj".txt | awk '{print $1}' | tail -1)
  # echo $number4
  #
  # number5=$(grep "win_DtiEpi_01_b10k" $scriptDir/acquisition_order_"$subj".txt | awk '{print $1}' | tail -1)
  # echo $number5


echo "done"

done

#!/bin/bash

# Before use:
# - edit your inputs to hMRI_wrapper below!
# - edit the path to the hMRI_wrapper script in the 'cd' command ad the end

# don't run this script directly, but call it using the following script:
# /vols/Scratch/neichert/myCode/LarynxCode/my_hMRI_wrapper_submit.sh
# the my_hMRI_wrapper_submit.sh script will submit the matlab function to the queue

# define function to pass the inputs to the hMRI_wapper
my_hMRI_wrapper(){
scan=$1

echo "do "$scan
matlab -nojvm -nodesktop -nosplash -r "hmri_wrapper_smallbore_Alberto('/vols/Scratch/premotor/MPM_rodent_data/"$scan"_NIFTI/SubjectDIR_RepetitionAverage','/vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline')"

}

# for map extraction
# hMRI_wrapper_Daniel('/vols/Data/MRdata/alazari/"$scan"','/home/fs0/alazari/spm12/', 1, 0, 0)
# for segmentation
# hMRI_segment('/vols/Scratch/alazari/longitudinal_paTMS/MPM/"$scan"')

# here the function is actually called
# $1 represents the input we are giving to my_hMRI_wrapper.sh (e.g. '01')
cd /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/
my_hMRI_wrapper $1
